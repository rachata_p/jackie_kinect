from __future__ import division
import numpy as np


#Constant variable list
#################################################

arow = 58.5	#Angle of view in row
acol = 45.6	#Angle of view in colum

imw = 640	#Image width
imh = 480	#Image height

#testcase
#     [ 0    1    2    3    4    5   ]

L   = [ 72  ,97  ,75  ,76  ,52  ,80  ] 
Xpx = [ 200 ,246 ,342 ,202 ,305 ,342 ] 
Ypx = [ 304 ,275 ,305 ,339 ,373 ,334 ]

x   = [0,0,0,0,0,0] 
y   = [0,0,0,0,0,0]


#################################################

#distance from half screen
def df_hs(D ,aov ,depth_reso ,xy ) :
    A = D/np.cos(np.deg2rad(aov/2))
    B = np.sqrt( (A**2) - (D**2) )                     
    C = xy - (depth_reso/2)           
    D = (C/depth_reso)*(2*B)         
    return round(D,3)


def CalDist(Xa,Xb ,Ya,Yb ,Da,Db) :
    
    distance = np.sqrt( ( (Xa-Xb)**2) + ( (Ya-Yb)**2) + ( (Da-Db)**2) )
   
    return round(distance,3)	


def L_to_distance(L):
    Distance = (L*0.638)+113.57 #Dstep = 0.638    D1=113.57  see algor in A4 paper
    return round(Distance,3)


#Distance in metre
D = [0]*6
y_h = [0]*6
x_h = [0]*6


for i in range(6) :
    D[i]      = L_to_distance(L[i])
    y_h[i] = df_hs( D[i] ,acol ,imh , Ypx[i] ) #distance y    half of screen 
    x_h[i] = df_hs( D[i] ,arow ,imw , Xpx[i] )
    print ('x = %.3f '%(x_h[i]))
    print ('y = %.3f '%(y_h[i]))


#RcL = dynamic resolution ratio(colum)     RrL = dynamic resolution ratio(row)
   

#now i got all xyz in cm unit
#i calculate distance by euclidean
length1 = np.sqrt( ((x_h[0]-x_h[1])**2)    +   ((y_h[0]-y_h[1])**2)    +   ((D[0]-D[1])**2)    )
width1  = np.sqrt( ((x_h[3]-x_h[4])**2)    +   ((y_h[3]-y_h[4])**2)    +   ((D[3]-D[4])**2)    )
height1 = np.sqrt( ((x_h[0]-x_h[3])**2)    +   ((y_h[0]-y_h[3])**2)    +   ((D[0]-D[3])**2)    )

#calculate volume 
volume = height1*width1*length1


print(' Dimention component')
print(' width1  : %.3f cm' %(width1)  )
print(' length1 : %.3f cm' %(length1) )
print(' height1 : %.3f cm' %(height1) )
print(' box valume = %.3f cm^3'%volume)

#size condition
if volume > 10000 and volume < 15000:
        print(' boxsize = big')
elif volume > 5000 :
        print(' boxsize = mid')
elif volume > 1000 :
        print(' boxsize = small')
else :
        print(' boxsize = unknown') 