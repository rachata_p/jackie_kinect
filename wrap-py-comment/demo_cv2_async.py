#!/usr/bin/env python
import freenect
import cv2
import frame_convert2

cv2.namedWindow('Depth') 
cv2.namedWindow('RGB')
keep_running = True


def display_depth(dev, data, timestamp): #ฟังชันแสดงข้อมูลdepth data 
    global keep_running #ให้keeprunningเป็นตัวแปรร่วมกับฟังชั่นอื่นๆ
    cv2.imshow('Depth', frame_convert2.pretty_depth_cv(data)) #คำสั่งแสดงภาพ
    if cv2.waitKey(10) == 27: #กดescเพื่อหยุดโปรแกรม
        keep_running = False


def display_rgb(dev, data, timestamp): #ฟังชันแสดงข้อมูลRGB
    global keep_running 
    cv2.imshow('RGB', frame_convert2.video_cv(data))
    if cv2.waitKey(10) == 27:
        keep_running = False


def body(*args): #ฟังชันที่ไม่ทราบจำนวนตัวแปร
    if not keep_running:
        raise freenect.Kill #เมื่อเกิดข้อผิดพลาด ให้รัน .kill เพื่อปิดการทำงานของโปรแกรม


print('Press ESC in window to stop')
freenect.runloop(depth=display_depth, # runloop = สั่งให้ทำงาน
                 video=display_rgb,
                 body=body)