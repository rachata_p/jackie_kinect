![Screenshot_from_2019-03-31_11-52-12](/uploads/184889f90b133d0ac5aad1e347ef357d/Screenshot_from_2019-03-31_11-52-12.png)

รูปเทส sorting points




![Screenshot_from_2019-03-31_13-09-07](/uploads/c5e06c5befcfc212fe34ef5ba5737ee2/Screenshot_from_2019-03-31_13-09-07.png)

รูปเทส หาเส้นที่ยาวที่สุดแต่ห่างกันมากกว่า 15 degree



![Screenshot_from_2019-03-31_14-20-07](/uploads/45295099cf52225800d9ccd522d7c3eb/Screenshot_from_2019-03-31_14-20-07.png)
เทสกับรูปthresholdedsize 640*480

list of sorting point by distance
contour element = 128

sort pnt 0     is point : 85   XY = (431, 332)   distance = 69.72087205421343

sort pnt 1     is point : 84   XY = (429, 332)   distance = 69.46221994724903

sort pnt 2     is point : 86   XY = (431, 331)   distance = 68.73136110975832

sort pnt 3     is point : 83   XY = (428, 331)   distance = 68.3593446428504

sort pnt 4     is point : 82   XY = (427, 331)   distance = 68.26419266350405

sort pnt 5     is point : 127  XY = (434, 197)   distance = 67.26812023536856

sort pnt 6     is point : 81   XY = (426, 330)   distance = 67.1863081289633

sort pnt 7     is point : 80   XY = (425, 330)   distance = 67.11929677819934

sort pnt 8     is point : 87   XY = (433, 329)   distance = 67.08203932499369

sort pnt 9     is point : 0    XY = (432, 197)   distance = 66.91038783328041

sort pnt 10    is point : 88   XY = (433, 328)   distance = 66.09841147864296

sort pnt 11    is point : 79   XY = (424, 329)   distance = 66.06814663663572

sort pnt 12    is point : 78   XY = (423, 329)   distance = 66.03029607687671

sort pnt 13    is point : 125  XY = (437, 199)   distance = 65.96969000988257

sort pnt 14    is point : 1    XY = (431, 198)   distance = 65.76473218982953

sort pnt 15    is point : 126  XY = (436, 199)   distance = 65.73431371817918

sort pnt 16    is point : 2    XY = (430, 198)   distance = 65.62011886609167

sort pnt 17    is point : 89   XY = (434, 327)   distance = 65.30696746902278


sort pnt 18    is point : 77   XY = (422, 328)   distance = 65.00769185258002

sort pnt 19    is point : 76   XY = (421, 328)   distance = 65.0

sort pnt 20    is point : 90   XY = (434, 326)   distance = 64.32728814430156

sort pnt 21    is point : 123  XY = (441, 202)   distance = 64.19501538281614

sort pnt 22    is point : 74   XY = (419, 327)   distance = 64.03124237432849

sort pnt 23    is point : 75   XY = (420, 327)   distance = 64.00781202322104

sort pnt 24    is point : 124  XY = (440, 202)   distance = 63.89053137985315

sort pnt 25    is point : 3    XY = (428, 200)   distance = 63.387695966961914

sort pnt 26    is point : 121  XY = (444, 204)   distance = 63.324560795950255

sort pnt 27    is point : 4    XY = (427, 200)   distance = 63.28506932918696

sort pnt 28    is point : 72   XY = (417, 326)   distance = 63.12685640834652

sort pnt 29    is point : 73   XY = (418, 326)   distance = 63.071388124885914

sort pnt 30    is point : 122  XY = (443, 204)   distance = 62.96824596572466

sort pnt 31    is point : 70   XY = (415, 325)   distance = 62.289646009589745

sort pnt 32    is point : 5    XY = (426, 201)   distance = 62.20128616033595

sort pnt 32    is point : 71   XY = (416, 325)   distance = 62.20128616033595

sort pnt 33    is point : 5    XY = (426, 201)   distance = 62.20128616033595

sort pnt 33    is point : 71   XY = (416, 325)   distance = 62.20128616033595

sort pnt 34    is point : 119  XY = (448, 207)   distance = 62.16912416947821

sort pnt 35    is point : 6    XY = (425, 201)   distance = 62.12889826803627

sort pnt 36    is point : 91   XY = (437, 323)   distance = 62.0966987850401

sort pnt 37    is point : 120  XY = (447, 207)   distance = 61.741396161732524

sort pnt 38    is point : 68   XY = (413, 324)   distance = 61.5223536610881

sort pnt 39    is point : 69   XY = (414, 324)   distance = 61.40032573203501

sort pnt 39    is point : 117  XY = (452, 210)   distance = 61.40032573203501

sort pnt 40    is point : 69   XY = (414, 324)   distance = 61.40032573203501

sort pnt 40    is point : 117  XY = (452, 210)   distance = 61.40032573203501

sort pnt 41    is point : 92   XY = (437, 322)   distance = 61.1310068623117

sort pnt 42    is point : 118  XY = (451, 210)   distance = 60.90155991434045

sort pnt 43    is point : 66   XY = (411, 323)   distance = 60.8276253029822

sort pnt 44    is point : 67   XY = (412, 323)   distance = 60.67124524847005

sort pnt 45    is point : 116  XY = (455, 213)   distance = 60.4648658313239

sort pnt 46    is point : 64   XY = (409, 322)   distance = 60.207972893961475

sort pnt 47    is point : 7    XY = (423, 203)   distance = 60.03332407921454

sort pnt 48    is point : 65   XY = (410, 322)   distance = 60.01666435249463

sort pnt 49    is point : 8    XY = (422, 203)   distance = 60.00833275470999

sort pnt 50    is point : 93   XY = (439, 320)   distance = 59.77457653551382

sort pnt 51    is point : 62   XY = (407, 321)   distance = 59.665735560705194

sort pnt 52    is point : 63   XY = (408, 321)   distance = 59.43904440685432

sort pnt 53    is point : 60   XY = (405, 320)   distance = 59.20304046246274

sort pnt 54    is point : 10   XY = (420, 204)   distance = 59.00847396772772

sort pnt 55    is point : 9    XY = (421, 204)   distance = 59.0


sort pnt 56    is point : 61   XY = (406, 320)   distance = 58.9406481131655

sort pnt 57    is point : 58   XY = (403, 319)   distance = 58.82176467941097

sort pnt 57    is point : 94   XY = (439, 319)   distance = 58.82176467941097

sort pnt 58    is point : 58   XY = (403, 319)   distance = 58.82176467941097

sort pnt 58    is point : 94   XY = (439, 319)   distance = 58.82176467941097

sort pnt 59    is point : 44   XY = (389, 312)   distance = 58.52349955359813

sort pnt 59    is point : 56   XY = (401, 318)   distance = 58.52349955359813

sort pnt 59    is point : 59   XY = (404, 319)   distance = 58.52349955359813

sort pnt 60    is point : 44   XY = (389, 312)   distance = 58.52349955359813

sort pnt 60    is point : 56   XY = (401, 318)   distance = 58.52349955359813

sort pnt 60    is point : 59   XY = (404, 319)   distance = 58.52349955359813

sort pnt 61    is point : 44   XY = (389, 312)   distance = 58.52349955359813

sort pnt 61    is point : 56   XY = (401, 318)   distance = 58.52349955359813

sort pnt 61    is point : 59   XY = (404, 319)   distance = 58.52349955359813

sort pnt 62    is point : 46   XY = (391, 313)   distance = 58.309518948453004

sort pnt 62    is point : 54   XY = (399, 317)   distance = 58.309518948453004

sort pnt 63    is point : 46   XY = (391, 313)   distance = 58.309518948453004

sort pnt 63    is point : 54   XY = (399, 317)   distance = 58.309518948453004

sort pnt 64    is point : 57   XY = (402, 318)   distance = 58.18934610390462

sort pnt 65    is point : 48   XY = (393, 314)   distance = 58.180752831155424

sort pnt 65    is point : 52   XY = (397, 316)   distance = 58.180752831155424


sort pnt 66    is point : 48   XY = (393, 314)   distance = 58.180752831155424

sort pnt 66    is point : 52   XY = (397, 316)   distance = 58.180752831155424

sort pnt 67    is point : 50   XY = (395, 315)   distance = 58.137767414994535

sort pnt 68    is point : 43   XY = (387, 310)   distance = 58.008620049092706

sort pnt 69    is point : 45   XY = (390, 312)   distance = 57.982756057296896

sort pnt 70    is point : 55   XY = (400, 317)   distance = 57.9396237474839

sort pnt 71    is point : 47   XY = (392, 313)   distance = 57.8013840664737

sort pnt 72    is point : 53   XY = (398, 316)   distance = 57.77542730261716

sort pnt 73    is point : 49   XY = (394, 314)   distance = 57.706152185014034

sort pnt 74    is point : 51   XY = (396, 315)   distance = 57.697486947006624

sort pnt 75    is point : 12   XY = (417, 206)   distance = 57.14017850864661

sort pnt 76    is point : 11   XY = (418, 206)   distance = 57.07889277132134

sort pnt 77    is point : 95   XY = (442, 316)   distance = 57.0087712549569

sort pnt 78    is point : 14   XY = (415, 207)   distance = 56.32051136131489

sort pnt 79    is point : 13   XY = (416, 207)   distance = 56.22277118748239

sort pnt 80    is point : 96   XY = (442, 315)   distance = 56.08029957123981

sort pnt 81    is point : 97   XY = (444, 313)   distance = 55.036351623268054

sort pnt 82    is point : 16   XY = (412, 209)   distance = 54.74486277268398

sort pnt 83    is point : 15   XY = (413, 209)   distance = 54.589376255824725

sort pnt 84    is point : 98   XY = (444, 312)   distance = 54.12947441089743

sort pnt 85    is point : 18   XY = (409, 211)   distance = 53.36665625650534

sort pnt 86    is point : 99   XY = (446, 310)   distance = 53.23532661682466

sort pnt 87    is point : 17   XY = (410, 211)   distance = 53.150729063673246


sort pnt 88    is point : 20   XY = (407, 212)   distance = 52.88667128870941

sort pnt 89    is point : 19   XY = (408, 212)   distance = 52.630789467763066

sort pnt 90    is point : 100  XY = (446, 309)   distance = 52.354560450833695

sort pnt 91    is point : 22   XY = (404, 214)   distance = 51.86520991955976

sort pnt 92    is point : 36   XY = (384, 227)   distance = 51.62363799656123

sort pnt 93    is point : 21   XY = (405, 214)   distance = 51.54609587543949

sort pnt 94    is point : 101  XY = (449, 306)   distance = 51.31276644267

sort pnt 95    is point : 24   XY = (401, 216)   distance = 51.07837115648854

sort pnt 96    is point : 34   XY = (387, 225)   distance = 50.99019513592785

sort pnt 97    is point : 35   XY = (385, 227)   distance = 50.91168824543142

sort pnt 98    is point : 23   XY = (402, 216)   distance = 50.695167422546305

sort pnt 99    is point : 28   XY = (396, 219)   distance = 50.60632371551998

sort pnt 99    is point : 32   XY = (390, 223)   distance = 50.60632371551998

sort pnt 100   is point : 28    XY = (396, 219)   distance = 50.60632371551998

sort pnt 100   is point : 32    XY = (390, 223)   distance = 50.60632371551998

sort pnt 101   is point : 26    XY = (398, 218)   distance = 50.53711507397311

sort pnt 102   is point : 30    XY = (393, 221)   distance = 50.47771785649585

sort pnt 102   is point : 102   XY = (449, 305)   distance = 50.47771785649585

sort pnt 103   is point : 30    XY = (393, 221)   distance = 50.47771785649585

sort pnt 103   is point : 102   XY = (449, 305)   distance = 50.47771785649585

sort pnt 104   is point : 33    XY = (388, 225)   distance = 50.32891812864648

sort pnt 105   is point : 27    XY = (397, 219)   distance = 50.11985634456667

sort pnt 106   is point : 25    XY = (399, 218)   distance = 50.08991914547278

sort pnt 107   is point : 31    XY = (391, 223)   distance = 50.0

sort pnt 107   is point : 103   XY = (451, 303)   distance = 50.0

sort pnt 108   is point : 31    XY = (391, 223)   distance = 50.0


sort pnt 108   is point : 103   XY = (451, 303)   distance = 50.0

sort pnt 109   is point : 29    XY = (394, 221)   distance = 49.92995093127971

sort pnt 110   is point : 104   XY = (451, 302)   distance = 49.20365840057018

sort pnt 111   is point : 105   XY = (453, 300)   distance = 48.91829923454004

sort pnt 112   is point : 106   XY = (453, 299)   distance = 48.16637831516918

sort pnt 113   is point : 107   XY = (455, 297)   distance = 48.08326112068523

sort pnt 114   is point : 109   XY = (458, 293)   distance = 47.634021455258214

sort pnt 115   is point : 108   XY = (455, 296)   distance = 47.38143096192854

sort pnt 116   is point : 41    XY = (386, 292)   distance = 45.45327270945405

sort pnt 117   is point : 42    XY = (387, 293)   distance = 45.34313619501854

sort pnt 118   is point : 114   XY = (456, 237)   distance = 43.60045871318328

sort pnt 119   is point : 115   XY = (455, 236)   distance = 43.41658669218482

sort pnt 120   is point : 110   XY = (458, 279)   distance = 40.311288741492746

sort pnt 121   is point : 111   XY = (457, 278)   distance = 39.0

sort pnt 122   is point : 37    XY = (384, 253)   distance = 38.3275357934736

sort pnt 123   is point : 38    XY = (385, 254)   distance = 37.107950630558946

sort pnt 123   is point : 39    XY = (385, 272)   distance = 37.107950630558946

sort pnt 124   is point : 38    XY = (385, 254)   distance = 37.107950630558946


sort pnt 124   is point : 39    XY = (385, 272)   distance = 37.107950630558946

sort pnt 125   is point : 40    XY = (386, 273)   distance = 36.40054944640259

sort pnt 126   is point : 112   XY = (457, 259)   distance = 36.22154055254967


sort pnt 127   is point : 113   XY = (456, 258)   distance = 35.35533905932738



