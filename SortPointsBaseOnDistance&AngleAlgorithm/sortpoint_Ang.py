import numpy as np

#cherprang
cX = 421
cY = 263
#fake contour for test
cnvt_cont = ((300,200),(250,450),(500,400),(100,50),(200,100),(1100,990))
sizearray = len(cnvt_cont)

#get all """coordinate"""  was created by 'points of contour' - 'cherprang'
pre_points = [[0,0]]*sizearray
points     = [[0,0]]*sizearray

pre_d	   = [[0,0]]*sizearray
ori_real_d = [[0,0]]*sizearray
sort_rd    = [[0,0]]*sizearray

for i in range(sizearray):
	#find points
	pre_points[i][0] = cnvt_cont[i][0] - cX
	pre_points[i][1] = cnvt_cont[i][1] - cY
	points[i]        = [pre_points[i][0],pre_points[i][1]] #original point
	#calculate distance
	pre_d[i][0]      = ((cnvt_cont[i][0] - cX)**2)
	pre_d[i][1]      = ((cnvt_cont[i][1] - cY)**2)
	ori_real_d[i]    = np.sqrt(pre_d[i][0] + pre_d[i][1]) #original distance
	sort_rd[i]       = np.sqrt(pre_d[i][0] + pre_d[i][1]) #prepare sorted   distance

sort_rd.sort(reverse=True)

#find coordinate x,y which keep most distance
#prepare array
sorted_points = [[0,0]]*sizearray #prepare for find angle

print('original cont')
print(cnvt_cont)
print('origninal dist')
print(ori_real_d)

#loop for find x,y
for i in range(sizearray):
	for j in range(sizearray):
		if sort_rd[i] == ori_real_d[j]:		#when index original = 5
			sorted_points[i] = cnvt_cont[j]	#contour index = 5 same
			print('sorted point')
			print(j)			#print contour index = 1 2 3 ... 130 (x,y)
			print(sorted_points[i])

#then i get all point this origin at 'cherprang'
#print(cnvt_cont)
#print(points)

def get_angle(p0,p1):
    	''' compute angle (in degrees) for p0p1p2 corner
    	Inputs:
        p0,p1,p2 - points in the form of [x,y]
    	'''
    	v0 = np.array(p0)
    	v1 = np.array(p1)

	angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
	print np.degrees(angle)
	return np.degrees(angle)



#algorithm to find 6 ext points of hexagon
maximumline = [[0,0]]*10 #10 cuase need to do this algorithm again    may be?

for l in range(sizearray):  #loop all point in contour
	AngBtwLine = get_angle(point[i],point[j])
	if AngBtwLine > 15 :
		print('i = ',i)
		print('j = ',j)

		maximumline[i] = point[i]

		i = j
		j = i+1
		print('i = ',i)
		print('j = ',j)
	else:
		i = i
		j = j+1