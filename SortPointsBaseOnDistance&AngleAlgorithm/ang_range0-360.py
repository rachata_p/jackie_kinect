import math
import imutils
import cv2
import numpy as np

#draw
cirthick  = 3
linethick = 3

color1 = (255,0,0) 			#color for cher
color2 = (0,255,0)			#color for dot
color3 = (0,0,255)			#color for line
color4 = (255,50,50)			#color for text
color5 = (50,255,50)

#img = cv2.imread('/home/jackie13/Pictures/threshold/thed2.jpg')
#img = cv2.imread('/home/jackie13/Pictures/threshold/minith2.jpg')
#img = cv2.imread("/home/jackie13/Pictures/threshold/minithed.png")
img = cv2.imread('/home/jackie13/Pictures/threshold/tresholded.png')

gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray_image,127,255,0)

#contouring and tuple that
im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)	#print(contours)
pre_cont  = np.vstack(contours).squeeze()	 						#print(pre_cont)
cnvt_cont = tuple(tuple(x) for x in pre_cont) 							#print(cnvt_cont)
#any line for pyimgsearch for ymax ymin and xmax
#must be edit later  never need to find contour 2 round
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cc = max(cnts, key=cv2.contourArea)


#find pun & cherprang
for c in contours:
	M = cv2.moments(c)
#pun
cX = int(M["m10"] / M["m00"])
cY = int(M["m01"] / M["m00"])
#cv2.circle(img, (cX, cY), 2, color1, -1)
#cv2.putText(img, "CHER", (cX - 25, cY - 25),cv2.FONT_HERSHEY_SIMPLEX, 0.5, color4, 2)
#cherprang
Ymax = tuple(cc[cc[:, :, 1].argmin()][0]) #Ymax have X1
Ymin = tuple(cc[cc[:, :, 1].argmax()][0]) #Ymin have X2 want to average two X
newCx = (Ymax[0]+Ymin[0])/2
newCenter = tuple((newCx,cY)) #Cherprang FOUND!



#REFERENCE AXIS = [newcenter]---------->[Xmax,cY]
Xmax = tuple(cc[cc[:, :, 0].argmax()][0])
start_ref = newCenter
end_ref   = [Xmax[0],cY] #if not add cY refline not 0 degree

#then i get reference line to
ref_line = start_ref,end_ref

#angle between 2 line case Y of contour OVER newCx
def dot(vA, vB):
	return vA[0]*vB[0]+vA[1]*vB[1]
def ang(lineA, lineB):
	# Get nicer vector form
    	vA = [(lineA[0][0]-lineA[1][0]), (lineA[0][1]-lineA[1][1])]
    	vB = [(lineB[0][0]-lineB[1][0]), (lineB[0][1]-lineB[1][1])]
    	# Get dot prod
    	dot_prod = dot(vA, vB)
    	# Get magnitudes
    	magA = dot(vA, vA)**0.5
    	magB = dot(vB, vB)**0.5
    	# Get cosine value
    	cos_ = dot_prod/magA/magB
    	# Get angle in radians and then convert to degrees
    	angle = math.acos(dot_prod/magB/magA)
    	# Basically doing angle <- angle mod 360
    	ang_deg = math.degrees(angle)%360
	ang_deg = round(ang_deg,5)
	if lineB[1][1] >= cY :	#this angle in range 0-180
		return 360-ang_deg
	else:
		return ang_deg

#draw line(origin at newCenter) and dot
def CircleLine(xymax):
	cv2.circle(img, (xymax), cirthick, color2, -1)
	cv2.line(img, (newCenter), (xymax), color3, linethick)




#point array
hexa =((384, 227),(389, 312),(434, 197),(431, 332),(452, 210),(458, 293))
#all_vect = [[0,0]]*sizearray

#line array
line_hexa = [[0,0]]*len(hexa)


#angle array
angjackie = [[0,0]]*len(hexa)
ang_str = [0]*len(hexa)


for i in range(len(hexa)):
	line_hexa[i] = (newCenter,hexa[i]) #assume it corect     need to find arg in ang fuction
	print line_hexa[1][1]
#	if line_hexa[1][1] >= cY
		#angle case Y over centerX
	angjackie[i] = ang(ref_line,line_hexa[i])
	ang_str[i]   = str(angjackie[i])
#	else :
		#angle case Y under centerY
#		angjackie[i] = ang(ref_line,)


#draw dot and line
	CircleLine(tuple(hexa[i]))

	#write angle into image
	cv2.putText(img, ang_str[i], hexa[i] ,cv2.FONT_HERSHEY_SIMPLEX, 0.5, color4, 2)

print(angjackie)







#display the image
cv2.imshow("Image", img)
cv2.waitKey(0)