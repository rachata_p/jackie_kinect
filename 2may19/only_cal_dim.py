from __future__ import division
import freenect
import cv2
import numpy as np
import math
import keyboard
import time

#the first line i import for eg 1/10 = 0  becuz i can run libfreenect in py2


#Constant variable list#################################################

#6_POINTS var###########
cirthick    = 4
linethick   = 3
color1      = (205,0,0) 			#color for center
color2      = (125,27,255)			        #color for dot
color3      = (0,0,205)			        #color for line
color4      = (0,150,50)			#color for text
color5      = (50,255,50)
font        =  cv2.FONT_HERSHEY_SIMPLEX
#the adjustable pixel for other left,right
adj_pixel   = 15


#cal_length var#########

aov_x = 58.5	#Angle of view in row
aov_y = 45.6	#Angle of view in colum

imw = 640	#Image width
imh = 480	#Image height

#testcase
#     		[ 0    1    2    3    4    5   ]
"""
L   = np.array( [ 72  ,97  ,75  ,76  ,52  ,80  ] )
Xpx = np.array( [ 200 ,246 ,342 ,202 ,305 ,342 ] ) 
Ypx = np.array( [ 304 ,275 ,305 ,339 ,373 ,334 ] )
"""
#funtion################################################

#function to get RGB image from kinect
def get_video():
    array,_ = freenect.sync_get_video()
    array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array
#function to get depth image from kinect
def get_depth():
    array,_ = freenect.sync_get_depth()
    array = array.astype(np.uint8)
    return array


def mask_depth_roi(dep):
	#first i mask depth
        lower =	15
        upper = 170
        dep2 = 255 * np.logical_and(dep > lower, dep < upper)
        mask_depth = dep2.astype(np.uint8)

	#then i make roi 
	black = np.zeros((mask_depth.shape[0], mask_depth.shape[1], 3), np.uint8)
	black1 = cv2.rectangle(black,(110,180),(390,310),(255, 255, 255), -1)   	#---the dimension of the ROI
	
	gray = cv2.cvtColor(black,cv2.COLOR_BGR2GRAY)               		#---converting to gray
	_,mask_roi = cv2.threshold(gray,127,255, 0)                 		#---converting to binary image

	#result is "roi AND depth" mask
	final_mask = cv2.bitwise_and(mask_roi, mask_roi, mask = mask_depth)
	return final_mask    #return all mask in roi              for find contour in next step

def seek_5points(biggest_cont, dep): #this function will give me     Xpx Ypx  and L

        #convert ;dfls'a list (nested list? maybe) to tuple
        pre_cont     = np.vstack(biggest_cont).squeeze()	 						
        cnvt_cont    = tuple(tuple(x) for x in pre_cont) 							
        cont_size    = len(cnvt_cont)

        #EASY 4 POINTS#############################################################################################################


        Xmin   = tuple(biggest_cont[biggest_cont[:, :, 0].argmin()][0])
        Xmax   = tuple(biggest_cont[biggest_cont[:, :, 0].argmax()][0])
        Ymin   = tuple(biggest_cont[biggest_cont[:, :, 1].argmin()][0])		    
        Ymax   = tuple(biggest_cont[biggest_cont[:, :, 1].argmax()][0])	

	#CENTER####################################################################################################################
        # center = start + length/2
        cX = Xmin[0] + ((Xmax[0] - Xmin[0])/2) 
        cY = Ymin[1] + ((Ymax[1] - Ymin[1])/2)
        center_cont = (cX,cY)

	#LEFT_POINTS_in zone _AND__RIGHT_POINTS in zone############################################################################
        #prepare array
        left_points  = [[0,0]]*cont_size
        right_points = [[0,0]]*cont_size


        #seek the contour who not far from left,right         
	AVG_left_X  = 0
	AVG_right_X = 0
	all_left_Y  = []
	all_right_Y = []

	left_count  = 0
	right_count = 0

	for i in range(cont_size) :
		if (cnvt_cont[i][0] - Xmin[0] <= adj_pixel) :	#far from Xmin < 15 pixel    
			AVG_left_X  = AVG_left_X + int(cnvt_cont[i][0])	 # add value for find average		
			left_count  = left_count + 1		
                        all_left_Y.append(cnvt_cont[i][1]) 
        
		if (Xmax[0] - cnvt_cont[i][0] <= adj_pixel) :
			AVG_right_X = AVG_right_X + int(cnvt_cont[i][0])			
			right_count = right_count + 1			
                        all_right_Y.append(cnvt_cont[i][1]) 
    
        Y_tl = min(all_left_Y)
        Y_bl = max(all_left_Y)
 
        Y_tr = min(all_right_Y)
        Y_br = max(all_right_Y)

	X_left  = int(AVG_left_X/left_count)
	X_right = int(AVG_right_X/right_count)

        #index, value = max(enumerate(all_left_Y), key=operator.itemgetter(1))

	topleft = (X_left,Y_tl) 
	botleft = (X_left,Y_bl)

	topright = (X_right,Y_tr) 
	botright = (X_right,Y_br)

	xy_px = np.array([ topleft ,topright,			#now i got bot_left   top_right
			   botleft ,botright  ])	

        L = []
        for x in range( (int(xy_px[0][0])+10),(int(xy_px[1][0])-10) ) :
                L.append(depth[int(xy_px[2][1])][x]) # i can select Y_bl  or Ybr  not important   both are useful 
        
        Lmin = min(L)
        min_index = L.index(Lmin)
        
        X_mid = (int(xy_px[0][0])+10) + min_index
        Y_mid = int(Y_bl) +5 

        botmid = (X_mid,Y_mid)

        xy5 = np.array( [ topleft ,topright ,
                          botleft ,botright ,botmid  ])
        #print xy5
        return xy5

#distance from half screen
def xy_cm(L ,aov ,depth_reso ,xy ) : #this function run 10 time( i have 5 point    each pts have X and Y)  
    Dist = (L*0.154)+58.4 #range < 110cm 			#Dstep = 0.638    D1=113.57  see algor in A4 paper		
    #Dist = (L*0.526)+104 #range   110cm+
    A = Dist/np.cos(np.deg2rad(aov/2))
    B = np.sqrt( (A**2) - (Dist**2) )                     
    C = xy - (depth_reso/2)           
    D = (C/depth_reso)*(2*B)         
    
    return round(D,3) #D= point in cm unit

def main_dimen(xy5 ,depth):
	x_cm = [0]*5
	y_cm = [0]*5
        L_cm = [0]*5
        
        x_0 = ((int(xy5[0][0]))+5 ) #push point into more accurate depth
        y_0 = ((int(xy5[0][1]))+5 )
            
        x_1 = ((int(xy5[1][0]))-5 )
        y_1 = ((int(xy5[1][1]))+5 )
        
        x_2 = ((int(xy5[2][0]))+5 )
        y_2 = ((int(xy5[2][1]))-5 )
        
        x_3 = ((int(xy5[3][0]))-5 )
        y_3 = ((int(xy5[3][1]))-5 )
        
        x_4 = (int(xy5[4][0])    ) 
        y_4 = ((int(xy5[4][1]))-5 )
        
        
        L = np.array( [ depth[ y_0 ][ x_0 ],     #depth[y][x]
                        depth[ y_1 ][ x_1 ],
                        depth[ y_2 ][ x_2 ],
                        depth[ y_3 ][ x_3 ],
                        depth[ y_4 ][ x_4 ]  ] )
        print ('L not cm')
        print L

	#at first    let find where  x,y in cm coordinate
	for i in range(5) :
		x_cm[i] = xy_cm(L[i] ,aov_x ,imw ,xy5[i][0] )	
		y_cm[i] = xy_cm(L[i] ,aov_y ,imh ,xy5[i][1] )
                L_cm[i] = (L[i]*0.154)+58.4    #range < 110cm
                #L_cm[i] = (L[i]*0.526)+104   #range   110cm+
                print ('L in cm')
                print (L_cm[i])

        height_a = np.sqrt(  ((x_cm[0]-x_cm[2])**2)  + ((y_cm[0]-y_cm[2])**2) + ((L_cm[0]-L_cm[2])**2)  )    
        height_b = np.sqrt(  ((x_cm[1]-x_cm[3])**2)  + ((y_cm[1]-y_cm[3])**2) + ((L_cm[1]-L_cm[3])**2)  )    
        
        height = (height_a+height_b)/2

        width  = np.sqrt(  ((x_cm[2]-x_cm[4])**2)  + ((y_cm[2]-y_cm[4])**2) + ((L_cm[2]-L_cm[4])**2)  )     
        lenght = np.sqrt(  ((x_cm[4]-x_cm[3])**2)  + ((y_cm[4]-y_cm[3])**2) + ((L_cm[4]-L_cm[3])**2)  )       
	
        volume = height*width*lenght 

        print('h = %.3f   w = %.3f  l = %.3f'%(height,width,lenght))
        
        if volume >= 3800 : 
        	sizee = 'big'
	elif volume <= 3799 and volume >= 1359 :
        	sizee = 'medium'
	elif volume <= 1358  :
        	sizee = 'small'
	else :
        	sizee = 'box size out of range' 
	
        print ('volumee = %.3f      box size : %s'%(volume,sizee))
	
        return sizee
	        

	#at last     specify size
	sizee(volume)	

do_once  = True
#MAIN
while 1:
    frame = get_video()
    depth = get_depth()
    
    masked      = mask_depth_roi(depth)
    result = cv2.bitwise_and(depth, depth, mask = masked)

    _, contours, _ = cv2.findContours(masked,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    biggest_cont = max(contours, key = cv2.contourArea)          #biggest contour is box contour
    
    #if keyboard.is_pressed('c'):  # i will change to  band alert later
    #print('STOP PRESSING!!!')            
    #print('You Pressed c Key! wait 5sec then i start calculate')
        
    if do_once == True :
        do_once  = False
        count_10 = 0
        
    if count_10 <= 10 :
        print count_10
        xy5 = seek_5points(biggest_cont, depth)    
        thesize = main_dimen(xy5, depth)
        #do_once = False
        count_10 = count_10 +1
        
    
    
    """
    
    if do_once == True:
        do_once = False
        print ('depth @y=270 in range x(121,275)')
        for x in range(121,275) :
            print depth[270][x]
    """ 
    cv2.imshow('RGB image',frame)
    cv2.imshow('Depth image',depth)
    cv2.imshow('Depth mask',masked)	
    cv2.imshow('result',result)        

    # quit program when 'esc' key is pressed
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break
cv2.destroyAllWindows() 