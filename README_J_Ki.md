https://openkinect.org/wiki/Getting_Started
https://pterneas.com/2016/08/11/measuring-distances-kinect/

![สารบัญ](/uploads/ca6bad29825cfcd0e5fba35a181bc455/สารบัญ.png)
# อ่านก่อนเริ่ม
    เอกสารเหล่านี้บอกว่าจะเริ่มใช้openkinectยังไง librariesพวกนี้เปลี่ยนไปเรื่อย และมันยังไม่เสร็จดี
คือคำแนะนำเหล่านี้มันอาจจะล้าสมัยไปละ ลองดูข้อมูลเหล่านี้ก่อนก็ได้
* https://github.com/OpenKinect/libfreenect#readme //น่าจะเกี่ยวกับทุกๆอย่างที่จำเป็นสำหรับkitect
* wrapper //ภาษาที่ใช้ได้กับ kitec
![Wrappers](/uploads/b37fbbe1cfccece8294eceb49a98184c/Wrappers.png)
* mgroups.google.com/forum/#!forum/openkinect //เมลที่ส่งมาถามผู้พัฒนา
* FAQ https://openkinect.org/wiki/FAQ

## Fakenect
* สามารถใช้ fakenect จำลอง kinect โดยไม่ต้องมี kinect ก็ได้

# Linux
## Ubuntu/Debian
### Official packages
* สามารถ install ด้วยคำสั่ง $ sudo apt-get install freenect
* ใน ubuntu 12.04 มันจะห้ามlibfreenect อ้างสิทธิ์อุปกรณ์ Kinect ดังนั้นเราก็ลบมันซะ
* $ sudo modprobe -r gspca_kinect 
* $ sudo modprobe -r gspca_main
* $ echo "blacklist gspca_kinect" |sudo tee -a /etc/modprobe.d/blacklist.conf
* หรือไม่ก็ติดตั้ง libfreenect ตัวล่าสุดที่ถอดไดรเวอร์เคอเนลออกเอง

ตัว freenect เอง สามารถเข้าถึงผู้ใช้ในกลุ่ม plugdev เอาจิงผู้ใช้ทั่วไปก็รวมอยุ่ในกลุ่มplugdev
แต่ถ้าต้องการaddเพิ่มไปในกลุ่ม ก็ใช้คำสั่ง
$ sudo adduser $USER plugdev  พิมเสดละก้ log out log inใหม่

## NeuroDebian repository
* หากต้องการใช้ libfreenect เวอร์ชั่นล่าสุด 
* เพื่อจะอนุญาติ NeuroDebian respository พิมคำสั่งนี้
* $ wget -O- http://neuro.debian.net/lists/$(lsb_release -cs).us-nh | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
* $ sudo apt-key adv --recv-keys --keyserver pgp.mit.edu 2649A5A9
* $ sudo apt-get update
* ติดตั้ง libfreenect เหมือนก่อนนี้
* $ sudo apt-get install freenect
* ละก็ต้องมั่นใจว่าอยุ่ในกลุ่มplugdevแล้ว (อยุ่ในกลุ่มเพื่อจะได้ใช้ freenect ได้)

## libtisch PPA //คือไรวะ
* เพื่อจะใช้ lib พิมคำสั่ง
* $ sudo add-apt-repository ppa:floe/libtisch
* $ sudo apt-get update
* ละก็พิม $ sudo apt-get install libfreenect libfreenect-dev libfreenect-demos
* คือพวกนี้จะinstall libfreenect,the development header,demo applications
* หลังจากนี้ คุณต้องaddคุณเข้าสู่กลุ่ม 'video' แล้วlog inกลับไป 
* ตัวpackageนี้ มีกฏที่จำเป็นสำหรับ udev daemon เพื่อให้สามารถเข้าถึงอุปกรณ์ได้สำหรับuser ในกลุ่มvideo
* $ sudo adduser $USER video ทำให้แน่ใจว่า log out แล้วก็ log inกลับไป ถ้าไม่อยากรีบูท แค่เสียบปลั้กkinect
(ถ้าเสียบอยู่ก็ถอดแล้วเสียบใหม่)

* เพื่อเริ่ม demo applications ใช้คำสั่ง $ freenect-glview

## Problems with accessing device
* เมื่อเกิดปัญหา พิมพ์ว่า $ lsusb | grep Xbox
* ซึ่งควรแสดง3อุปกรณ์
* lsusb | grep Xbox                                                
* Bus 001 Device 021: ID 045e:02ae Microsoft Corp. Xbox NUI Camera
* Bus 001 Device 019: ID 045e:02b0 Microsoft Corp. Xbox NUI Motor
* Bus 001 Device 020: ID 045e:02ad Microsoft Corp. Xbox NUI Audio
* ถ้าอุปกรณ์พวกนั้นไม่ขึ้น พิมคำสั่ง echo -1 | sudo tee -a /sys/module/usbcore/parameters/autosuspend
* แล้วก็reconnect Kinect ทีนี้ kinect cameraควรจะอยู่ในlistละ

## Ubuntu Manual Install
 * พิมคำสั่งตามนี้
 * sudo apt-get install git-core cmake libglut3-dev pkg-config build-essential libxmu-dev libxi-dev libusb-1.0-0-dev
 * git clone git://github.com/OpenKinect/libfreenect.git
 * cd libfreenect
 * mkdir build
 * cd build
 * cmake ..
 * make
 * sudo make install
 * sudo ldconfig /usr/local/lib64/
 * sudo freenect-glview

ถ้าขึ้นว่า error saying apt-get cannot find libglut แสดงว่าคุณน่าจะมีubuntuตัวใหม่กว่า 
ดังนั้น การ initial install apt จะต้องเป็นยังงี้
* sudo apt-get install git-core cmake freeglut3-dev pkg-config build-essential libxmu-dev libxi-dev libusb-1.0-0-dev

การใช้kinect สำหรับผู้ใช้แบบ non - root ทำตามนี้
* sudo adduser $USER video

สร้างไฟล์ร่วมกับกฏสำหรับ linux device managerด้วย
* sudo nano /etc/udev/rules.d/51-kinect.rules

Copy and paste
# ATTR{product}=="Xbox NUI Motor"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02b0", MODE="0666"
# ATTR{product}=="Xbox NUI Audio"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02ad", MODE="0666"
# ATTR{product}=="Xbox NUI Camera"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02ae", MODE="0666"
# ATTR{product}=="Xbox NUI Motor"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02c2", MODE="0666"
# ATTR{product}=="Xbox NUI Motor"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02be", MODE="0666"
# ATTR{product}=="Xbox NUI Motor"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02bf", MODE="0666"

แน่ใจนะว่า log outแล้วlog in ใหม่แล้ว

กรณีคุณไม่สามารถเข้าไปใช้งาน หรือ still need root privileges  เพื่อจะใช้งานอุปกรณ์ของคุณ  : ในบางกรณีอาจมีข้อขัดแย้งระหว่างสิทธิ์ของ2driverที่เราลงไป(libfreenect and primesense)
ถ้านี่คือกรณีของคุณ ลองติดตั้งไดรเวอร์เซ็นเซอร์ใหม่ของ primesense และเก็บกฎของ primesense ไว้เฉพาะไฟล์ /etc/udev/rules.d/55-primesense-usb.rules เอาไฟล์ /etc/udev/rules.d/51-kinect.rules หากคุณสร้างไฟล์ดังกล่าว


