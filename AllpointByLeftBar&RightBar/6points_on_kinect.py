import freenect
import cv2
import numpy as np
import math

#any_function#######################################################################################################

def drawCircle(xymax):
    cv2.circle(result, xymax, cirthick, color2, -1)
    cv2.circle(depth, xymax, cirthick, color2, -1)
    #cv2.circle(mask, xymax, cirthick, color2, -1)

def drawLine(startpoint,endpoint):
    cv2.line(result, startpoint, endpoint, color3, linethick)

def putText(giveme_string,giveme_xy):
    new_Y = giveme_xy[1] - 5 # i need text fly over x,y a bit
    cv2.putText(result, giveme_string, (giveme_xy[0],new_Y), font, 1, color4 ,1,cv2.LINE_AA)
    cv2.putText(depth, giveme_string, (giveme_xy[0],new_Y), font, 1, color4 ,1,cv2.LINE_AA)

#use this for cleaning array                    this function will tell you howmany not 0 in your array
def index0(list_in):
    l_size = 0
    for i in range(len(list_in)):
        if (list_in[i] != [0]):
	    l_size = l_size+1
	#print('list size should be : ')
	#print l_size
    return l_size
def index00(list_in):
    l_size = 0
    for i in range(len(list_in)):
        if (list_in[i] != 0):           #some array like angle_array cant use above fuction   then i create this
	    l_size = l_size+1
    return l_size
def index0_0(list_in):
    l_size = 0
    for i in range(len(list_in)):
	if (list_in[i] != [0,0]):       #this tooooo
	    l_size = l_size+1
    return l_size


def dot(vA, vB):
    return vA[0]*vB[0]+vA[1]*vB[1]

def ang(lineA, lineB):
    # Get nicer vector form
    vA = [(lineA[0][0]-lineA[1][0]), (lineA[0][1]-lineA[1][1])]
    vB = [(lineB[0][0]-lineB[1][0]), (lineB[0][1]-lineB[1][1])]
    # Get dot prod
    dot_prod = dot(vA, vB)
    # Get magnitudes
    magA = dot(vA, vA)**0.5
    magB = dot(vB, vB)**0.5
    # Get cosine value
    cos_ = dot_prod/magA/magB
    # Get angle in radians and then convert to degrees
    angle = math.acos(dot_prod/magB/magA)
    # Basically doing angle <- angle mod 360
    ang_deg = math.degrees(angle)%360
    #if "Y" is under "Y @ ref_vector" :
    if lineB[1][1] > lineA[1][1] :      
        ang_deg = 360 - ang_deg
    #if ang_deg-180>=0:
        # As in if statement
    #    return round(360 - ang_deg,2)
    #else:
    return round(ang_deg,3)


def avg_angle_lef(size , angle1, min_angle, max_angle):
    sum_angle = 0
    the_count = 0
    for i in range(size):
        if (cnvt_cont[i] != Xmin) :
            if (cnvt_cont[i][0] - Xmin[0] <= adj_pixel) :	
                if((angle1[i] > min_angle) & (angle1[i] < max_angle)): #range271-310
                    sum_angle = sum_angle + angle1[i]
                    the_count = the_count + 1
    
    avg_angle = 0
    if the_count != 0 :
        avg_angle = (sum_angle/the_count)
    #print ('avg angle : %f'%(avg_angle))
    return avg_angle

def avg_angle_righ(size , angle1, min_angle, max_angle):
    sum_angle = 0
    the_count = 0
    for i in range(size):
        if (cnvt_cont[i] != Xmax) :
            if (Xmax[0] - cnvt_cont[i][0] <= adj_pixel) :	
                if((angle1[i] > min_angle) & (angle1[i] < max_angle)): #range271-310
                    sum_angle = sum_angle + angle1[i]
                    the_count = the_count + 1
    
    avg_angle = 0       #for debug divided by zero
    if the_count != 0 :
        avg_angle = (sum_angle/the_count)
    return avg_angle


#function for slide bar
def nothing(x):
    pass

#function to get RGB image from kinect
def get_video():
    array,_ = freenect.sync_get_video()
    array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array
 
#function to get depth image from kinect
def get_depth():
    array,_ = freenect.sync_get_depth()
    array = array.astype(np.uint8)
    return array


#adjustable value#############################################################################################################

#draw
cirthick    = 4
linethick   = 3


color1      = (205,0,0) 			#color for center
color2      = (125,27,255)			        #color for dot
color3      = (0,0,205)			        #color for line
color4      = (0,150,50)			#color for text
color5      = (50,255,50)


font       =  cv2.FONT_HERSHEY_SIMPLEX
#font        =  cv2.FONT_HERSHEY_SCRIPT_COMPLEX


#the adjustable pixel for other left,right
adj_pixel   = 20


#cv2.namedWindow("Trackbars")
#cv2.createTrackbar("L - H", "Trackbars", 0, 179, nothing)
#cv2.createTrackbar("L - S", "Trackbars", 0, 255, nothing)
#cv2.createTrackbar("L - V", "Trackbars", 0, 255, nothing)
#cv2.createTrackbar("U - H", "Trackbars", 179, 179, nothing)
#cv2.createTrackbar("U - S", "Trackbars", 255, 255, nothing)
#cv2.createTrackbar("U - V", "Trackbars", 255, 255, nothing)

### START
if __name__ == "__main__":
    while 1:
        #PREPARE_CONTOURS###########################################################################################

        #get a frame from RGB camera
        frame = get_video()
        #get a frame from depth sensor
        depth = get_depth()

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#	l_h = cv2.getTrackbarPos("L - H", "Trackbars")
#	l_s = cv2.getTrackbarPos("L - S", "Trackbars")
#	l_v = cv2.getTrackbarPos("L - V", "Trackbars")
#	u_h = cv2.getTrackbarPos("U - H", "Trackbars")
#	u_s = cv2.getTrackbarPos("U - S", "Trackbars")
#	u_v = cv2.getTrackbarPos("U - V", "Trackbars")

#        lower_mask = np.array([l_h, l_s, l_v])   #value in my house      ...i hate sunshine 
#        upper_mask = np.array([u_h, u_s, u_v])


        lower_mask = np.array([0   , 0  , 137])   #value in my house      ...i hate sunshine 
        upper_mask = np.array([106 , 255, 255])

        mask   = cv2.inRange(hsv, lower_mask, upper_mask)
        result = cv2.bitwise_and(frame, frame, mask=mask)
	
        #contouring and tuple that
        im2, contours, hierarchy = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)	

        #the object MUST be biggest contour
        biggest_cont             = max(contours, key = cv2.contourArea)          #biggest contour is box contour

        #convert ;dfls'a list (nested list? maybe) to tuple
        pre_cont                 = np.vstack(biggest_cont).squeeze()	 						
        cnvt_cont                = tuple(tuple(x) for x in pre_cont) 							
        cont_size                = len(cnvt_cont)

        #EASY 4 POINTS#############################################################################################################

        Xmin   = tuple(biggest_cont[biggest_cont[:, :, 0].argmin()][0])
        Xmax   = tuple(biggest_cont[biggest_cont[:, :, 0].argmax()][0])
        Ymax   = tuple(biggest_cont[biggest_cont[:, :, 1].argmin()][0])		    
        Ymin   = tuple(biggest_cont[biggest_cont[:, :, 1].argmax()][0])		

        #note   Ymax = 74(top)     Ymin= 300(bottom)

        #CENTER####################################################################################################################

        # center = start + length/2
        cX = Xmin[0] + ((Xmax[0] - Xmin[0])/2) 
        cY = Ymin[0] + ((Ymin[0] - Ymax[0])/2)
        center_cont = (cX,cY)                     #may be use in convoyer system

        #REFERENCE_LINE############################################################################################################

        #ref_line1 = [Xmin]---------------->[cX ,Y @ Xmin]
        #ref_line2 = [cX ,Y @ Xmax]<--------[Xmax]

        start_ref1 = tuple(Xmin)
        end_ref1   = cX,Xmin[1]

        start_ref2 = tuple(Xmax)
        end_ref2   = cX,Xmax[1]

        ref_line1  = (start_ref1,end_ref1)
        ref_line2  = (start_ref2,end_ref2)
        
#if stable == 1 do it
        #LEFT_POINTS__AND__RIGHT_POINTS############################################################################################
        #prepare array
        left_points  = [[0]]*cont_size
        right_points = [[0]]*cont_size

        #seek the contour who not far from left,right 
        for i in range(cont_size) :
            if (cnvt_cont[i] != Xmin) :
                if (cnvt_cont[i][0] - Xmin[0] <= adj_pixel) :			        #far from Xmin < 10 pixel
                    left_points[i]  = cnvt_cont[i]         

            if (cnvt_cont[i] != Xmax) :
                if (Xmax[0] - cnvt_cont[i][0] <= adj_pixel) :		                #far from Xmax < 10 pixel
                    right_points[i] = cnvt_cont[i]             

        #ANGLE_LEFT&RIGHT##########################################################################################################

        #line array 
        line_left    = [[0]]*cont_size
        line_right   = [[0]]*cont_size 
        #create angle1 array                        #NOTE!! angle1 is all of angle in zone left and right
        angle1_left  = [0]*cont_size                #then   angle2 is angle in range 270-310 
        angle1_right = [0]*cont_size                #       !!!!     I USE angle2 to seeeekk true xy left,right    !!!!!
        #create angle2 array
        angle2_left  = [0]*cont_size
        angle2_right = [0]*cont_size


        #left angle
        for i in range(cont_size) :
            if (cnvt_cont[i] != Xmin) :
                if (cnvt_cont[i][0] - Xmin[0] <= adj_pixel) :	
                    line_left[i]    = (Xmin,left_points[i])
                    angle1_left[i]  = ang(ref_line1,line_left[i])

        #right angle
        for i in range(cont_size) :
            if (cnvt_cont[i] != Xmax) :
                if (Xmax[0] - cnvt_cont[i][0] <= adj_pixel) :	
                    line_right[i]   = (Xmax,right_points[i])
                    angle1_right[i] = ang(ref_line2,line_right[i])


        #want to know *repeatly  angle
        avg_angle_left  = avg_angle_lef(cont_size ,angle1_left  ,270 ,310)    
        avg_angle_right = avg_angle_righ(cont_size ,angle1_right ,270 ,310)



        #start find true xy
        #x,y coordinate who hold the angle2
        xy_left  = [[0,0]]*cont_size
        xy_right = [[0,0]]*cont_size


        #now this line start to seek "angle2"
        #angle2 on left side
        for i in range(cont_size):
            if (cnvt_cont[i] != Xmin) :
                if (cnvt_cont[i][0] - Xmin[0] <= adj_pixel) :	
                    if (   ((avg_angle_left - angle1_left[i])>0)&((avg_angle_left - angle1_left[i])< 10)   ):
                        angle2_left[i]  = angle1_left[i] 
                        xy_left[i]      = cnvt_cont[i]
        
        Y_left  = sorted(xy_left,reverse = True, key=lambda x: x[1])


        #angle2 on right side
        for i in range(cont_size):
            if (cnvt_cont[i] != Xmax) :
                if (Xmax[0] - cnvt_cont[i][0] <= adj_pixel) :	
                    if (   ((avg_angle_right - angle1_right[i])>0)&((avg_angle_right - angle1_right[i])< 10) ):
                        angle2_right[i] = angle1_right[i] 
                        xy_right[i]     = cnvt_cont[i]

        Y_right  = sorted(xy_right,reverse = True, key=lambda x: x[1])

        #6 POINTS###################################################################################################################

        topleft  = Xmin
        botleft  = tuple(Y_left[0])

        topmid   = Ymax
        botmid   = Ymin

        topright = Xmax
        botright = tuple(Y_right[0])


        drawCircle(topleft)
        putText('top_l',topleft)
        drawCircle(botleft)
        putText('bot_l',botleft)

        drawCircle(topmid)
        putText('top_m',topmid)
        drawCircle(botmid)
        putText('bot_m',botmid)

        drawCircle(topright)
        putText('top_r',topright)
        drawCircle(botright)
        putText('bot_r',botright)

        cv2.imshow('RGB image',frame)
        cv2.imshow('Depth image',depth)
        cv2.imshow("mask", mask)
        cv2.imshow("result", result)
                
        # quit program when 'esc' key is pressed
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break
        ### END 
    cv2.destroyAllWindows() 