#import the necessary modules
import freenect
import cv2
import numpy as np

#draw
cirthick    = 4

color2      = (125,27,255)			        #color for dot
color4      = (0,150,50)			#color for text

#font       =  cv2.FONT_HERSHEY_SIMPLEX
font        =  cv2.FONT_HERSHEY_SCRIPT_COMPLEX

xy = (300,300)


def drawCircle(xymax):
    cv2.circle(depth, xymax, cirthick, color2, -1)
def putText(giveme_string,giveme_xy):
    new_Y = giveme_xy[1] - 5 # i need text fly over x,y a bit
    cv2.putText(depth, giveme_string, (giveme_xy[0],new_Y), font, 1, color4 ,1,cv2.LINE_AA)

#function to get RGB image from kinect
def get_video():
    array,_ = freenect.sync_get_video()
    array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array
 
#function to get depth image from kinect
def get_depth():
    array,_ = freenect.sync_get_depth()
    array = array.astype(np.uint8)
    return array
 
if __name__ == "__main__":
    while 1:
        #get a frame from RGB camera
        frame = get_video()
        #get a frame from depth sensor
        depth = get_depth()
        

        text = ('depth = %d'%(depth[xy[0]][xy[1]]))
        drawCircle(xy)
        putText(text,xy)
        
        
        print depth[300][300]

        
        #display RGB image
        cv2.imshow('RGB image',frame)
        #display depth image
        cv2.imshow('Depth image',depth)
 
        # quit program when 'esc' key is pressed
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()