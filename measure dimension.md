**kinect วัดระยะทางยังไง https://pterneas.com/2016/08/11/measuring-distances-kinect/**
**body track by kinect https://pterneas.com/2014/03/13/kinect-for-windows-version-2-body-tracking/**

# basic kinect
**kinect มีเซนเซอir และ เซนเซอความลึก พื้นที่ๆkinectเห็น เรียกว่า “field of view”**
**โดย depth processor จะสร้าง depth frame ขนาด512×424จุด**
**ตัวkinect sdk(software development kit) จะสร้างdepth frameใส่algorithm ตรวจจับร่างกาย**
**แล้วมันจะคำนวนข้อต่อร่างกาย และ dimension**

**ข้อต่อแต่ละข้อจะมีค่า x y z เปนค่าแบบพิกัดคาเดเชี่ยน โดยจุดoriginal(0,0,0) อยู่ที่ตัวกล้อง(จุดอื่นๆทุกจุดจะถูกวัดจากเซนเซอที่ตัวกล้อง)**
**x = แกนนอน y = แกนตั้ง z = แกนลึก**
![overhead_of_kinect](/uploads/92052456e76e2450a0aaba013bdce195/overhead_of_kinect.png)**


## แกนZ
**ค่า z คือระยะทางจาก ระนาบหน้ากล้อง ถึง วัตถุ (เหมือนมีกำแพงด้านหน้ากล้อง)**
**ค่า z คือระยะทางแนวตั้งจากผนังนั้น**

**ตัวอย่างโค้ดที่ใช้ดึงค่า z**
**var distance = body.Joints[JointType.SpineBase].Position.Z;**

**ตัวแปร body คือตัวแปรที่ติดตามplayer (xbox) **
**position เป็นคุณสมบัติ(ฟังชั่นย่อย?)ที่ใช้เก็บ x y z **
**z = ระยะห่างระหว่าง player กับ ระนาบ **

## การวัดระยะ
**ระยะทางระหว่างระนาบ คือ เส้นสีฟ้าในรูป(อยู่ในรูปเวกเตอร์ทางคณิตศาสตร์)**
**ความยาวเส้นสีฟ้า^2 = x^2 + y^2 + z^2**

**ซึ่งsdkให้ค่าแล้ว ที่เราต้องทำคือยัดค่าใส่สูตร**

![Untitled](/uploads/f8a49c5f58c8432a83002eca63545162/Untitled.png)

co map : https://vitruviuskinect.com/coordinate-mapping/

kinect เข้าใจ3dได้จาก ir,depth sensor ตัวdepth processorสามารถคำนวนระยะห่างระหว่าง วัตถุ กับ ตัวกล้อง
Kinect SDK ใช้structureที่ชื่อว่า CameraSpacePoint เพื่อแสดงจุดในพื้นที่ 3D ทางกายภาพ
ใน CameraSpacePoint มีตัวแปร3ตัวคือ x y z xเกบค่าแกนนอน yแกนตั้ง zระยะห่างจากระนาบของกล้อง

ค่าxyzถูกวัดออกมาในหน่วย เมตร 
CameraSpacePoint [1.5, 2.0, 4.7] หมายถึง
ต0ำแหน่ง ณ 
1.5mจากด้านซ้ายของวัตถุ ,  2.0m จากด้านบนของวัตถุ , 4.7m จากระนาบของตัวกล้อง
![ex_camspacepoint](/uploads/6cbcd80fa5920aa049806f8b88d23e52/ex_camspacepoint.png)

อย่างไรก็ตาม ในการพัฒนาapp kinect เราใช้ computer monitor 
เราฉายตำแหน่ง3d (3d point) ในจอ2d นั่นคือมี 2 screen-space
* Color Space: 1920×1080 pixels
* Depth/Infrared Space: 512×424 pixels

ชัดเจนว่า2d = x,y ซึ่งค่าx,y อยู่ในหน่วยของpixel ดังนั้นเราต้องแปลงหน่วยเมตรเป็น pixel 
ดูได้จาก https://pterneas.com/2014/05/06/understanding-kinect-coordinate-mapping/ (เดวค่อยแปล)

ต่อมา 